// File: speakerDriver
// Author: Aditya Ravi
// Input parameters: smpl, speaker_radd
speakerDriver   cp      0x80000042		smpl								//copy sample to speaker_sample
            	cp		0x80000040		const_num1							//set speaker_command to 1
spwait        	bne     spwait			0x80000041	const_num1				//wait till speaker_response is 1
       	 		cp  	0x80000040		const_num0							//set speaker_commmand to 0 so sample can be changed
spwait2	 		bne		spwait2			0x80000041	const_num0
end_speaker		ret     speaker_radd

smpl			0
speaker_radd	0		
