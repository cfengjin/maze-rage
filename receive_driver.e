// File: receive_driver.e
// Author: Ashwin Sreevasta
// Description: receive driver for serial and wireless communication

receive_driver	cp      0x80000090	const_num0				//reset command flag
				cp		0x80000090	const_num1				//set flag to 1
wait1			bne     wait1 		0x80000091	const_num1	//wait for response flag to be 1
receive_data	cp		output 		0x80000092				//copy output
				cp		0x80000090	const_num0				//set flag to 0
wait2			bne		wait2		0x80000091	const_num0	//wait for response flag to be 0
end_receive		ret		receive_radd						//return

output			0
receive_radd	0

