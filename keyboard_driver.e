// File: keyboard_driver.e
// Author: Kevin Beallis
// finds the ascii values of a key pressed on a ps2 keyboard
// Command parameters: none
// Response parameters: ps2_pressed (key press or release), ps2_ascii[7:0] (which key was pressed)
// Notes: Non-blocking driver implemented by Aditya Ravi

//This section will wait for the signal for a key pressed
keyboard_driver		cp	0x80000020	const_num1						// copies 1 to the command address
			

kdloop1		cp		transaction_key		0x80000021
			bne		kdend				0x80000021		const_num1	// waits for the response address to be 1
			cp		ps2_ascii			0x80000023					// stores ascii value of key
			cp		ps2_pressed			0x80000022					// grabbed pressed value for testing 
			cp		0x80000020			const_num0					// copies 0 to the command address

//This section will wait for the signal for a key released.
			cp		0x80000020			const_num1					// copies 1 to the command address
kdloop2		bne		kdloop2				0x80000021		const_num1	// waits for response address to be 1
						
			cp		ps2_pressed			0x80000022					// grabbed pressed value for testing 
			cp		0x80000020			const_num0					// copies 0 to the command address

//This section will wait for the signal to turn off
kdloop3		bne		kdloop3				0x80000021		const_num0	//waits for the response address to be 0
			cp		transaction_key		0x80000021
kdend		ret		ps2_radd										//returns to address specified in variable

ps2_pressed		0	 
ps2_ascii		0	
ps2_radd	    0
transaction_key 0	







