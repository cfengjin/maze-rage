// File: drawingAlgorithm.e
// Author: Aditya Ravi
// Description: Several functions that draw images/paths/etc. on VGA monitor
// Input Params: num_col, position, drawFinish, drawLoss drawing_radd


// INITIALIZES SCREEN TO BE WHITE (can be called seperately)
drawingInit         div         squareDim           const_num400    num_col         //find square dimension for each position

// sets the entire VGA screen background as black 
setBack	            cp			vga_x1				const_num0                      
                    cp			vga_y1				const_num0
                    cp			vga_x2				const_num639                    //Max VGA coordinate x
                    cp			vga_y2				const_num479                    //Max VGA coordinate y
                    cp			vga_write			const_num1
                    cp			vga_color			const_c_black
                    call		vga_driver			vga_radd
endBackgroundInit   ret         drawing_radd                                        //return address for screen initialization


// DRAWS POSITION AT INPUT POSITION
drawPosition        be          markPos             0              0                
markPos             cp          colIndex            position                        //copy column index from current position

computeCol          cp          mod_op1             position                        //set modulus operand               
                    cp          mod_op2             num_col                         //set modulus operand
                    call        modulo              modulo_radd                     //call modulus operator to find column 
                    cp          colIndex            mod                             //colIndex = position % num_col
                    mult        screenCol           colIndex       squareDim        //screenCol = colIndex*squareDim -- to find vga index
                    add         vga_x1              screenCol      const_num119     //vga_x1 = screenCol + 119 -- to get index within bounds

computeRow          sub         rowIndex            position       colIndex         //subtract to find rowIndex divisible by num_col
                    div         rowIndex            rowIndex       num_col          //rowIndex = rowIndex / num_col
                    sub         rowIndex            rowIndex       const_num1       //decrement rowIndex for proper maze indexing
                    mult        rowIndex            rowIndex       squareDim        //rowIndex = rowIndex*squareDim -- to find vga index
                    add         vga_y1              rowIndex       const_num39      //vga_y1 = rowIndex + 39 -- to get index within 400x400 bounds

drawPos             add         vga_x2              vga_x1         squareDim        //vga_x2 = vga_x1 + squareDim
                    add         vga_y2              vga_y1         squareDim        //vga_y2 = vga_y1 + squareDim
                    cp          vga_write           const_num1                      // set vga_write on
                    cp          vga_color           const_c_white                   // vga_color = white

checkFinish         bne         checkLoss           drawFinish   const_num1         //if finish flag raised, use green instead
                    cp          vga_color           const_c_green
                    be          callDriver          0            0

checkLoss           bne         callDriver          drawLoss     const_num1         //if loss flag raised, use red color instead
                    cp          vga_color           const_c_red
                    be          callDriver          0            0

callDriver          call        vga_driver          vga_radd                        //call vga driver to write color
                    be          endDrawing          0            0                  


endDrawing          ret         drawing_radd                                        //return 





#include vga_driver.e
#include modulo.e

drawFinish   0
drawLoss     0
modulo_radd  0
screenCol    0
squareDim    0
colIndex     0
rowIndex     0
drawing_radd	0