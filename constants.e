// constants.e
// holds constant values for assembly files
// name formats:
// decimal consts: const_num<number> <number>

const_numn1	-1
const_num0	0
const_num1	1
const_num2	2
const_num3	3
const_num4	4
const_num5	5
const_num6  6
const_num10	10
const_num11	11
const_num12	12
const_num15	15
const_num24 24
const_num30 30
const_num32	32
const_num39	39
const_num48	48
const_num64	64
const_num100    100
const_num119    119
const_num120	120
const_num128	128
const_num131	131
const_num250    250
const_num400    400
const_num479    479
const_num500    500
const_num639    639
const_num1000   1000
const_num2000   2000
const_num2500   2500
const_num3000   3000
const_num4000   4000
const_num8000   8000
const_num32000  32000   

// character consts: const_char<character>
const_charL	76
const_charR	82
const_charU	85
const_charD	68
const_char:	58

// keyboard consts: keyconst_<key> <ASCII value>

keyconst_sp	32
keyconst_w	119
keyconst_a	97
keyconst_e	101
keyconst_s	115	
keyconst_d	100
keyconst_r	114

// driver constants: <driver abbreviation>_<function>	<hexadigit>   <read or write>

keyboard_command	0x80000020	// write
keyboard_response	0x80000021	// read
keyboard_pressed	0x80000022	// read
keyboard_value		0x80000023	// read

speaker_command		0x80000040	// write
speaker_response	0x80000041	// read
speaker_sample		0x80000042	// write

mic_command		0x80000050	// write	
mic_response		0x80000051	// read
mic_value		0x80000052	// read

serial_receive_command	0x80000090	// write
serial_receive_response	0x80000091	// read
serial_receive_data	0x80000092	// read
serial_send_command	0x800000a0	// write
serial_send_response	0x800000a1	// read
serial_send_data	0x800000a2	// write

// color constants:
const_c_white   0xffffff
const_c_green   0xff00
const_c_red     0xff0000
const_c_black   0
