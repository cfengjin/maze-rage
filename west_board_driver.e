// File: west_board_driver.e
// Author: Kevin Beallis
// Description: High-level driver for west board that recieves sample data and delays
// Unique Version for each board

sample_loop	call    receive_driver	receive_radd					//call receive driver to get sample values
			cp      sample			output							//copy sample values
			cpta    sample			samples			samples_index	//copy sample values to array
			add     samples_index	samples_index   const_num1		//samples_index++
			blt     sample_loop		samples_index	const_num4000	// loop until 4000 samples collected 

delay_grab	call	receive_driver	receive_radd					//receive delays 
			cp		0x80000003		output
			cpta	output			delays			count			//delays[count] = output
			add		count			count			const_num1		//++count
			blt		delay_grab		count			const_num3		//collect 3 delays


check_echo	cp		0x80000003		const_num3
			call    receive_driver	receive_radd					//call receive driver, and echo if value is 1
			cp		0x80000003		output
			be      echo			output          const_num1		//if value is one, then echo
			be      check_end		output          const_num0		//else check if end of game

echo		cpfa    temp_delay		delays			const_num2		//copy the right delay
			be		no_delay		temp_delay		const_num1		//if temp_delay == 1 -> goto no_delay
			sl		dir_delay		temp_delay		const_num6		//shift delays back by 64 magnitude
			be		echo1			0			0					//goto echo1
no_delay	cp		dir_delay		const_num1						//dir_delay = 1
echo1		call    output_echo		output_echo_radd				//output the echo with the correct delay

outputer	cp		0x80000001		const_num119

check_end	call    receive_driver	receive_radd					//call receive driver to check status of game at end of turn
			cp		0x80000003		output	
			cp      count			const_num0 						//count = 0
			cp      samples_index	const_num0						//samples_index = 0       
			be      delay_grab		output          const_num0		//if 0, game is still going, so we grab delays again
			be      sample_loop		output          const_num1 		//if 1, restart, so need to grab samples
			be      end_game		output          const_num2		//if 2, end_game, so need to finish game

end_game	cp	0x80000001	const_numn1
			halt


#include receive_driver.e
#include output_echo.e
#include constants.e
#include speaker_driver.e

sample			0
samples_index   	0
sound_index		0
count   		0
temp_delay		0
delays  		0
        		0
        		0
samples 		0
