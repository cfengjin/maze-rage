// File: mic_driver.e
// Author: Fengjin Cui
// Description: call and read sound sample from microphone controller
// parameters: return value
// returns: sample

mic_driver	cp	0x80000050	const_num1			// turn on driver
check_on	bne	mic_driver	0x80000051	const_num1
collect		cp	mic_sample	0x80000052			// collect sample
check_data	be	collect		0x80000052	const_num0
turn_off	cp	0x80000050	const_num0			// turn off driver
check_off	bne	turn_off	0x80000051	const_num0
done		ret	mic_radd					// return

mic_sample	0
mic_radd    0


