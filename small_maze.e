//File: small_maze.e
// Author: Kevin Beallis
// holds the array for the static maze
// as well as some variables relevant to the maze

small_startpos	11	//starting index (0 indexed) - 2nd row, 2nd column. A 0 in the maze. Keep constant!ß
small_position	11	//Actual position in maze updated throughout execution.  same as startpos at beginning
small_num_rows	10	//number of rows in maze
small_num_col		10	//number of columns in maze

//The maze itself!
small_maze		1	//row1
		1
		1
		1
		1
		1
		1
		1
		1
		1

		1	//row2
		0
		1
		0
		0
		0
		0
		0
		0
		1

		1	//row3
		0
		0
		0
		0
		1
		1
		0
		0
		1

		1	//row4
		1
		1
		0
		0
		0
		0
		1
		1
		1
	
		1	//row5
		0	
		0
		0
		1
		0
		0
		0
		0
		1

		1	//row6
		0	
		1
		1
		0
		0
		1
		0	
		0
		1

		1	//row7
		0
		0
		1
		0
		1
		0
		1
		0
		1
	
		1	//row8
		1
		0
		0
		0
		0
		0
		0
		0
		1

		1	//row9
		0
		0
		0
		1
		1
		0		
		1
		2	//endpoint
		1
	
		1	//row10
		1
		1
		1
		1
		1
		1
		1
		1
		1

//Created by Kevin Beallis
