// File: send_echoes.e
// author: Fengjin Cui
// description: master board sends echoes simultaneously to all boards sequentially
// parameters: down_echo, left_echo, right_echo, send_echoes_radd
// returns: nothing


send_echoes		sr		temp_send		right_echo	const_num6	//shift echo value to fit in 8 bit s_data
send_right		cp		s_data			temp_send				//send right echo value (1 value)
				call	send_driver		send_radd				
				sr		temp_send		down_echo	const_num6	//shift down echo value to fit in 8 bit s_data
send_down		cp		s_data			temp_send				//send down echo value (1 value)
				call	send_driver		send_radd
				sr		temp_send		left_echo	const_num6	//shift left echo value to fit in 8 bit s_data
				cp		s_data			temp_send				//send left echo value (1 value)
				call	send_driver		send_radd
				ret 	send_echoes_radd

send_echoes_radd	0
s_data				0		
temp_send			0



