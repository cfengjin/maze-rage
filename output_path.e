// File: output_path.e
// Author: Aditya Ravi
// Description: Ouputs the path traveled until the end of the game

output_path be      copyLastp   0           0					
copyLastp	cpta	position	positions	positions_size		//positions[positions_size] = position
			add		positions_size	positions_size	const_num1	//++positions_size
outputPath	cp		drawFinish	const_num0						//drawFinish = 0
			cp		drawLoss	const_num0						//drawLoss = 0
			cpfa	position	positions	pos_idx				//position = positions[pos_idx]
			add		pos_idx		pos_idx		const_num1			//++pos_idx
			
			be		grab_small	maze_type	const_num0			//if small_maze, goto grab_small
grab_large	cpfa	nTValueGrab	large_maze	position			//get large_maze value at position
			be		end_grab	0			0					
grab_small	cpfa	nTValueGrab	small_maze	position			//get small_maze value at position
			
end_grab	bne		checkL  	nTValueGrab const_num2			//if value != 2 --> goto checkL
			cp		drawFinish	const_num1						//else drawFinish = 1
			be		outP1		0			0					//goto outP1

checkL		bne		outP1		nTValueGrab	const_num1			//if (value != 1) goto outP1
			cp		drawLoss	const_num1						//else drawLoss = 1
			be		outP1		0			0					//goto outP1

outP1		call	drawPosition	drawing_radd				//draw current position
			
// Visual delay of 1/16 second btwn drawings to show actual path to user on VGA
waittime1	cp		timeElap	0x80000005						
			add		drawDelay	timeElap	const_num500
waittime2	cp		timeElap	0x80000005
			blt   	waittime2	timeElap	drawDelay
			be		endoutput	positions_size		pos_idx
			be		outputPath	0			0

endoutput   ret     output_path_radd




#include drawingAlgorithm.e

output_path_radd		 0
timeElap	0
drawDelay	0
positions_size	0
pos_idx	0
positions	0

