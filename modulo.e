// File: modulo.e
// Author: Aditya Ravi
// Description: Computes modulo function (mod_op1 % mod_op2)
// Input Params: mod_op1, mod_op2, modulo_radd -- mod_op1 and mod_op2 are positive
// Output Params: mod

modulo  cp      mod         const_num0              //mod = 0
        blt     cpMod       mod_op1       mod_op2   // if (op1 < op2), (op1 % op2 = op1)
        be      endMod      mod_op1       mod_op2   // if (op1 == op2), (op1 % op2 = 0)

subm    sub     mod_op1     mod_op1       mod_op2   // while(op2 < op1) {op1 -= op2}
        be      endMod      mod_op1       mod_op2
        blt     subm        mod_op2       mod_op1
        

cpMod   cp      mod         mod_op1                 //mod = op1
endMod  ret     modulo_radd                         //return


mod_op1     0
mod_op2     0
mod         0