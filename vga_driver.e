// File: vga_driver.e
// Author: Aditya Ravi 
// VGA driver for visual screen output
// Input Params: vga_x1,vga_y1,vga_y2,vga_color, vga_read, vga_write
// vga_radd
// Outputs: VGA screen

// copy command parameters to values
vga_driver  cp 0x80000063   vga_x1                  
            cp 0x80000064   vga_y1
            cp 0x80000065   vga_x2
            cp 0x80000066   vga_y2
            cp 0x80000062   vga_write
            cp 0x80000067   vga_color  

            cp 0x80000060   const_num1              //set command flag on
waitvga     bne waitvga     0x80000061  const_num1  //wait till response flag is on
            bne end_vga     vga_write   const_num0  //if (vga_write == 1) --> end_vga

readScreen  cp  vga_color_val   0x80000068          // else get color at position(s)


end_vga     cp 0x80000060   const_num0              //turn off command flag
waitvga2    bne waitvga2    0x80000061  const_num0  //wait till response is also off
            ret vga_radd


vga_x1  0
vga_y1  0
vga_x2  0
vga_y2  0
vga_write   0
vga_color   0
vga_color_val   0
vga_radd    0