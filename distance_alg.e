// File: distance_alg.e
// Author: Aditya Ravi
// Input Parameters: <map_name>, position, indx_end, num_rows, num_col,distance_radd
// Output Parameters: LeftD, RightD, UpD, DownD
// Description: Calculates distance in each direction from given virtual position


// output parameters represent the distance to the nearest wall in each direction
// if nearest "wall" is the finish, then returns -1

distance_alg	be	reset_count1	0	0	

reset_count1	cp	right_count	const_num0	//right_count = 0
reset_count2	cp	left_count	const_num0	//left_count = 0
reset_count3	cp	up_count	const_num0	//up_count = 0
reset_count4	cp	down_count	const_num0	//down_count = 0

// Right Search         
search_right	add     right_count	right_count	const_num1	//increment right count
		add     index           right_count	position	//get index to find value 
                be      grab_small5     maze_type       const_num0      //if small maze, get small_maze index
grab_large5     cpfa    index_val       large_maze      index		//get value at index for large maze
                be      end_grab5       0               0                   
grab_small5     cpfa    index_val       small_maze      index		//get value at index for small maze
end_grab5       be      finishR         index_val	const_num2 	//if (index_val == 2), goto finishR
                be      end_searchR	index_val	const_num1	//if (index_val == 1), goto end_searchR
                be      search_right	0		0		//loop back if wall not found
finishR         cp      right_count	const_numn1			//right_count = -1 if finish direction
		be	end_searchR2	0		0
end_searchR	sub	right_count	right_count	const_num1	//decrement right_count for correct formatting
end_searchR2	call    reset_index	return				//reset the index back to the original position input
		be      search_left	0		0

// Left Search
search_left	add     left_count	left_count	const_num1	//increment left count
		sub     index           position	left_count	//get index to find value
                be      grab_small6     maze_type       const_num0      //if small maze, get small_maze index
grab_large6     cpfa    index_val       large_maze      index		//get value at index for large maze
                be      end_grab6       0               0
grab_small6     cpfa    index_val       small_maze      index		//get value at index for small maze
end_grab6       be      finishL		index_val	const_num2	//if (index_val == 2), goto finishL
                be      end_searchL	index_val	const_num1	//if (index_val == 1), goto end_searchL
                be      search_left	0		0		//loop back if wall not found
finishL         cp      left_count	const_numn1			//left_count = -1 if finish direction
		be	end_searchL2	0		0
end_searchL	sub	left_count	left_count	const_num1	//decrement left_count for correct formatting
end_searchL2	call    reset_index	return				//reset the index back to the original position input
		be      search_up	0		0

// Up search
search_up	add     up_count	up_count	const_num1	//increment up count
		mult	vert_temp	num_col		up_count	//get positional displacement    
		sub     index           position	vert_temp	//get index to find value
                be      grab_small7     maze_type       const_num0      //if small maze, get small_maze index
grab_large7     cpfa    index_val       large_maze      index		//get value at index for large maze
                be      end_grab7       0               0
grab_small7     cpfa    index_val       small_maze      index		//get value at index for small maze
end_grab7       be      finishU         index_val	const_num2	//if (index_val == 2), goto finishU
                be      end_searchU	index_val	const_num1      //if (index_val == 1), goto end_searchU
                be      search_up	0		0		//loop back if wall not found
finishU         cp      up_count	const_numn1		        //left_count = -1 if finish direction
		be	end_searchU2	0		0	
end_searchU     sub	up_count	up_count	const_num1	//decrement up_count for correct formatting
end_searchU2	call    reset_index	return				//reset the index back to the original position input
		be      search_down	0		0

// Down search
search_down	add     down_count	down_count	const_num1	//increment down count
		mult	vert_temp	num_col		down_count	//get positional displacement
		add     index           position	vert_temp	//get index to find value
                be      grab_small8     maze_type       const_num0      
grab_large8     cpfa    index_val       large_maze      index		//get value at index
                be      end_grab8       0               0
grab_small8     cpfa    index_val       small_maze      index		//get value at index
end_grab8       be      finishD		index_val	const_num2	//if (index_val == 2), goto finishD
                be      end_searchD	index_val	const_num1	//if (index_val == 1), goto end_searchD
                be      search_down	0		0		//loop back if wall not found
finishD         cp      down_count	const_numn1			//set left_count = -1 if finish direction
		be	end_searchD2	0		0
end_searchD	sub	down_count	down_count	const_num1	//decrement down_count for correct formatting
end_searchD2	be      display1        0		0		//reset the index back to the original position input

display1	cp	curr_position	position	                //current_position = position
display2	call	displayDist	dispDist_radd	                //call display function
endDist         ret     distance_radd

reset_index	cp      index           position	                //index = position
		ret	return				                //return


return		0	//return address for reset_index call
index           0	//index of map
index_val       0	//value of index
vert_temp	0	//temp placeholder to get vertical displacement from position

//Output parameters
left_count	0	
right_count	0
up_count	0
down_count	0

distance_radd		0


#include displayDist.e

