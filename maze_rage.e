// maze_rage.e
// Top-level function for game
// Authors: Aditya Ravi and Fengjin Cui


maze_rage	call	load_maze		load_maze_radd			// load specific maze based on SW[0]
backg		call	drawingInit		drawing_radd			// initialize black background screen.
logo		call	drawLogo		drawLogo_radd			// draw logo on screen
			call	mic_record		mic_record_radd			// records voice and sends to other boards 
backg2		call	drawingInit		drawing_radd			// redraw black background on VGA
			cp		position		startpos				// resets position to starting position
			cp		positions_size	const_num0				// positions_size = 0
			cp		pos_idx			const_num0				// pos_idx = 0
 

game_loop	be		ending_selection	endgame		const_num1	// check if game is over
			call	game_simulator		game_simulator_radd		// call turn simulator
			be		game_loop			0			0			// return back to loop

ending_selection	be		restart		ending_type	const_num1	// if loss, restart
					be		finish_game	ending_type	const_num2	// if win, finish game

restart				cp		endgame		const_num0			// reset endGame status
					cp		ending_type	const_num0			// reset ending_type status
					call	output_path	output_path_radd	// output path of user
					cp		end_timer	0x80000005			// wait for four seconds
					add   	end_delay	end_timer	const_num32000

end_wait			cp		end_timer	0x80000005
					blt   	end_wait 	end_timer	end_delay
            		be		maze_rage	0			0		//return back to game loop

finish_game			call	output_path	output_path_radd


end					cp		0x80000001	const_numn1
					halt									//end of game

end_delay	0
end_timer	0

#include drawLogo.e
#include load_maze.e
#include constants.e
#include game_simulator.e
#include mic_record.e
#include output_path.e
