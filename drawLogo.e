// File: drawLogo.e
// Author: Aditya Ravi
// Input params: sd driver
// Description: Draws Maze Rage logo along with starting instruction to VGA monitor

drawLogo   cp          x_coor      x_init                   //x_coor = x_init
           cp          y_coor      y_init                   //y_coor = y_init
           cp          address     const_num0               //address = 0


logoLoop    be         endLogo     y_coor      y_max        //if (y_coor == y_max) -> logo drawing is done
            cp         sd_write    const_num0               //set sd status to read only
            cp         sd_address  address                  //sd_address = address
            call       sd_driver   sd_radd                  //get value at sd_address from SD card
            cp         vga_color   sd_data                  //vga_color = sd[address]
            cp         vga_x1      x_coor                   //vga_x1 = x_coor
            cp         vga_y1      y_coor                   //vga_y1 = y_coor
            add        vga_x2      x_coor      const_num1   //vga_x2 = x_coor + 1
            add        vga_y2      y_coor      const_num1   //vga_y2 = y_coor + 1   
            cp         vga_write   const_num1               //set vga status to write
            call       vga_driver  vga_radd                 //draw pixel (x1y1,x2y2,color)
            add        x_coor      x_coor      const_num1   //++x_coor
            add        address     address     const_num1   //++address
            bne        logoLoop    x_coor      x_max        //if (x_coor != x_max) -> goto logoLoop
            cp         x_coor      x_init                   //else x_coor = x_init
            add        y_coor      y_coor      const_num1   //++y_coor
            be         logoLoop    0           0            //goto logoLoop

endLogo     ret        drawLogo_radd



drawLogo_radd   0
address  0
x_init   48
y_init   123
x_max    593
y_max    357
x_coor   0
y_coor   0

#include sd_driver.e

