// File: send_driver.e
// Author: Ashwin Sreevasta
// Description: driver for sending in serial and wireless communication

send_driver	cp		0x800000a0	const_num0				//reset driver
			cp		0x800000a2	s_data					//copy temp to send_data
send_data	cp		0x800000a0	const_num1				//Set flag to 1
swait1		bne		swait1		0x800000a1	const_num1 	//wait for response flag of 1
			cp		0x800000a0	const_num0				//set flag to 0
swait2		bne		swait2		0x800000a1	const_num0	//wait for response flag of 0 
			ret		send_radd							//return

send_radd	0
