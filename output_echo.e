// Author: Aditya Ravi and Kevin Beallis
// File: output_echo.e
// Description: File on each board that outputs sound file with specified delay
// Input Parameters: dir_delay, samples, output_echo_radd
// Outputs: Outputs echo to speaker


output_echo		cp		time			0x80000005  				//wait for given delay before output
				be    	end_echo		dir_delay		const_num1
				add     finish_time		time			dir_delay

delay_loop		cp      time			0x80000005  				//wait for given delay before output
				blt     sound_out		finish_time		time 
				be  	delay_loop		0				0

sound_out		be      end_echo		sound_index		const_num4000	//play through 4000 samples
				cpfa    temp_samples	samples			sound_index		//temp_samples = samples[sound_index]
				sl		smpl 			temp_samples	const_num24		//smpl = left shift temp_samples
				call 	speakerDriver	speaker_radd					//output smpl
				add     sound_index		sound_index		const_num1		//++sound_index
				be      sound_out		0				0				

end_echo		cp		sound_index		const_num0						//sound_index = 0
				ret     output_echo_radd

dir_delay			0
temp_samples		0
time				0
finish_time			0
output_echo_radd	0
