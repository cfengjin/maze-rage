// File: sd_driver.e
// Author: Kevin Beallis
// Description: Driver for Initialized SD card
// Input Parameters: sd_write, sd_address,sd_data_write,sd_radd
// Outputs: sd_data 

sd_driver	cp		0x80000082	sd_write				
			cp		0x80000083	sd_address	
			bne		command_on	sd_write	const_num1
write_data	cp		0x80000084	sd_data_write
command_on	cp		0x80000080	const_num1
sd_wait1	bne		sd_wait1	0x80000081	const_num1
read_data	cp		sd_data		0x80000085    
			cp		0x80000080	const_num0
sd_wait2	bne		sd_wait2	0x80000081	const_num0
			ret		sd_radd

sd_data         0
sd_write        0
sd_address      0
sd_data_write   0
sd_radd			0


