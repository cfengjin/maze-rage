// File: lcd_driver.e
// Author: Aditya Ravi
// Description: Driver for lcd output
// input params: lcd_x, lcd_y, lcd_ascii, lcd_radd
// output params: output on lcd

lcd_driver	cp		0x80000012	lcd_x					//copy x-coordinate value
			cp 		0x80000013	lcd_y					//copy y-coordinate value
			cp		0x80000014	lcd_ascii				//copy ascii value of LCD output
			cp		0x80000010	const_num1				//set command to on
wait		bne		wait		0x80000011	const_num1	//wait till response signal is one
			cp		0x80000010	const_num0				//set command signal off to reset params in future calls
wait2		bne		wait2		0x80000011	const_num0	//wait till response signal is back off
endLCD		ret		lcd_radd



lcd_x		0
lcd_y		0
lcd_ascii	0
lcd_radd	0




