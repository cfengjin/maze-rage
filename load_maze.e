// File: load_maze.e
// Author: Aditya Ravi
// Description: Loads the specified maze based on SW[0]
// if SW[0] = 1 --> hard maze, otherwise small maze loaded

load_maze   be   load_small   0x80000000         const_num0     //if (SW[0] == 0) --> load small   

load_large  cp   startpos     large_startpos                    //startpos = large_startpos
            cp   position     large_position                    //position = large_position
            cp   num_rows     large_num_rows                    //num_rows = large_num_rows
            cp   num_col      large_num_col                     //num_col = large_num_col
            cp   maze_type    const_num1                        //maze_type = 1 (represents large maze)
            be   endLoad      0                  0              //goto end

load_small  cp   startpos     small_startpos                    //startpos = small_startpos
            cp   position     small_position                    //position = small_position
            cp   num_rows     small_num_rows                    //num_rows = small_num_rows 
            cp   num_col      small_num_col                     //num_col = small_num_col
            cp   maze_type    const_num0                        //maze_type = 0 (represents small maze)

endLoad     ret  load_maze_radd

maze_type       0
position        0
startpos        0
num_rows        0
num_col         0
load_maze_radd  0

#include small_maze.e
#include large_maze.e
