// File: echo_algorithm.e
// Author: Fengjin Cui
// description: master board calculates delays before transmission
// One unit of distance corresponds to a 4000 clock cycle (0.5 second) delay in sound output/echo
// parameters: up_count, down_count, left_count, right_count
// returns: up_echo, down_echo, left_echo, right_echo


echo_algorithm	be		no_right	right_count	const_numn1		//if (right_count == -1) goto no_right
				mult	right_echo	right_count	const_num4000	//else right_echo = 4000*right_count
				be		check_down	0			0				//goto check_down
no_right		cp		right_echo	const_num64					//right_echo = 64

check_down		be		no_down		down_count	const_numn1		//if (down_count = -1) goto no_down
				mult	down_echo	down_count	const_num4000	//else down_echo = 4000*down_count
				be		check_left	0			0				//goto check_left
no_down			cp		down_echo	const_num64					//down_echo = 64

check_left		be		no_left		left_count	const_numn1		//if (left_count = -1) goto no_left
				mult	left_echo	left_count	const_num4000	//else left_echo = 4000*left_count
				be		check_up	0			0				//goto check_up
no_left			cp		left_echo	const_num64					//left_echo = 64

check_up		be		no_up		up_count	const_numn1		//if (up_count = -1) goto no_up
				mult	up_echo		up_count	const_num4000	//else up_echo = 4000*up_count
				be		end_echo_c	0			0				//goto check_up
no_up			cp		up_echo		const_num64					//up_echo = 64

end_echo_c		ret		echo_algorithm_radd


right_echo	0
left_echo	0
down_echo	0
up_echo		0
echo_algorithm_radd	0



