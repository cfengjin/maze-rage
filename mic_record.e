// File: mic_record.e
// Authors: Aditya Ravi and Fengjin Cui
// Description: Records voice, collects first 4000 samples for initial sound pickup, and sends to other boards
// input parameters: mic_record_radd
// output parameters: samples

//NOTE: It is necessary to wait after sending 1000 samples because receive driver does not
// process values quickly enough, and the buffer for serial send fills up. It sufficies
// to delay approximately 500 clock cycles every 1000 samples sent



mic_record		call	keyboard_driver	ps2_radd                        //wait till 'R' pressed on keyboard
				be      kdloop1			transaction_key	const_num1
				bne     mic_record		ps2_ascii		keyconst_r
				cp      size			const_num0				        //reset size to 0 to overwrite sample values

mic_record2		call    mic_driver		mic_radd				        //read mic samples
				sr      temp_data		mic_sample		const_num24     //shift temp data back 24 bits to 8 bit value
            
writeMic		cpta    temp_data		samples			size            //samples[size] = temp_data
				add     size			size			const_num1	    //++size
				be      send_samples	size			const_num4000	//stop after 4000 samples collected
				be      mic_record2		0				0               //else loop to mic_record2

//Send the values - Wait for 500 clock cycles every 1000 samples sent
send_samples    cp		sound_index		const_num0                      //sound_index = 0
send_samples1	cpfa    s_data  		samples  		sound_index     //s_data = samples[sound_index]
                add     sound_index  	sound_index  	const_num1      //++sound_index
                call    send_driver 	send_radd                       //send sample value
                cp      mod_op1 		sound_index                     //wait to send values after 1000 samples
                cp      mod_op2 		const_num1000
                call    modulo  		modulo_radd
                be      send_wait    	mod     		const_num0    
check_send      blt     send_samples1  	sound_index  	const_num4000   //send only 4000 samples
                be      end_record     	0       		0   


// timer that waits for 1/16 of second before returning to continue sending values
send_wait       cp      timer       	0x80000005                      
                add     delay       	timer       	const_num500
send_wait2      cp      timer       	0x80000005
                blt     send_wait2      timer       	delay
                be      check_send     	0           	0


end_record		cp	    0x80000003		const_num1
				ret     mic_record_radd

temp_data	             0
size		             0
sound_index	             0
timer		             0
delay		             0
mic_record_radd          0

#include mic_driver.e
#include samples.e
