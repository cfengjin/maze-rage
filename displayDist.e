// File: displayDist.e
// Author: Aditya Ravi
// input params: up_count, down_count, left_count, right_count, dispDist_radd
// output params: displays to lcd
// Description: displays distances to walls (only for testing purposes)

displayDist	add	upAscii		up_count		const_num48	//get digit ascii value for each directional count
			add	downAscii	down_count   	const_num48
			add	rightAscii	right_count  	const_num48
			add	leftAscii	left_count   	const_num48

//Outputs left wall distance in format "L:<dist>"
displayL1	cp	lcd_x		const_num0
displayL2	cp	lcd_y		const_num0	
displayL3	cp	lcd_ascii	const_charL	
displayL4	call	lcd_driver	lcd_radd	
displayL5	cp	lcd_x		const_num1	
displayL6	cp	lcd_y		const_num0
displayL7	cp	lcd_ascii	const_char:
displayL8	call	lcd_driver	lcd_radd
displayL9	cp	lcd_x		const_num2
displayL10	cp	lcd_y		const_num0
displayL11	cp	lcd_ascii	leftAscii
displayL12	call	lcd_driver	lcd_radd

//Outputs right wall distance in format "R:<dist>"
displayR1	cp	lcd_x		const_num10
displayR2	cp	lcd_y		const_num0
displayR3	cp	lcd_ascii	const_charR
displayR4	call	lcd_driver	lcd_radd
displayR5	cp	lcd_x		const_num11
displayR6	cp	lcd_y		const_num0
displayR7	cp	lcd_ascii	const_char:
displayR8	call	lcd_driver	lcd_radd
displayR9	cp	lcd_x		const_num12
displayR10	cp	lcd_y		const_num0
displayR11	cp	lcd_ascii	rightAscii
displayR12	call	lcd_driver	lcd_radd

//Outputs up wall distance in format "U:<dist>"
displayU1	cp	lcd_x		const_num0
displayU2	cp	lcd_y		const_num1
displayU3	cp	lcd_ascii	const_charU
displayU4	call	lcd_driver	lcd_radd
displayU5	cp	lcd_x		const_num1
displayU6	cp	lcd_y		const_num1
displayU7	cp	lcd_ascii	const_char:
displayU8	call	lcd_driver	lcd_radd
displayU9	cp	lcd_x		const_num2
displayU10	cp	lcd_y		const_num1
displayU11	cp	lcd_ascii	upAscii
displayU12	call	lcd_driver	lcd_radd

//Outputs down wall distance in format "D:<dist>"
displayD1	cp	lcd_x		const_num10
displayD2	cp	lcd_y		const_num1
displayD3	cp	lcd_ascii	const_charD
displayD4	call	lcd_driver	lcd_radd
displayD5	cp	lcd_x		const_num11
displayD6	cp	lcd_y		const_num1
displayD7	cp	lcd_ascii	const_char:
displayD8	call	lcd_driver	lcd_radd
displayD9	cp	lcd_x		const_num12
displayD10	cp	lcd_y		const_num1
displayD11	cp	lcd_ascii	downAscii
displayD12	call	lcd_driver	lcd_radd

endDispDist	ret	dispDist_radd

curr_position	0
leftAscii	0
rightAscii	0
upAscii		0
downAscii	0
dispDist_radd	0	//return address for dispDist

#include lcd_driver.e
