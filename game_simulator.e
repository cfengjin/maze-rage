// File: game_simulator.e
// Author: Aditya Ravi and Kevin Beallis
// Simulates the game at every turn
// input parameters: maze, game_simulator_radd, position, position_size
// output parameters: end_game, end_game_type

game_simulator	call    distance_alg	distance_radd				//Calculate distances from current position
                cp      ps2_ascii	const_num0				//set ps2_ascii pressed to 0 
                cpta   	position	positions		positions_size	//positions[positions_size] = position
		add	positions_size	positions_size	        const_num1      //positions_size++                 

calc_delays	call    echo_algorithm	echo_algorithm_radd	                //calculates delays for each direction
		call    send_echoes	send_echoes_radd	                //send echoes with sr by 4 bits

//no serial send in this section
key_loop	call	keyboard_driver	ps2_radd				//Get keyboard input
		be	kdloop1 	transaction_key		const_num1	//go back to driver if keyboard ready
               	be      echo            ps2_ascii		keyconst_sp	//output echos if space pressed
                be	setw		ps2_ascii		keyconst_w	//directional components
		be	seta		ps2_ascii		keyconst_a	
		be	sets		ps2_ascii		keyconst_s	
		be	setd		ps2_ascii		keyconst_d	
                be	key_loop	0			0


echo            cp      0x80000002	const_num0                      //set LED_GREEN off
                cp      s_data		const_num1                      //put 1 in the send data
                call    send_driver	send_radd                       //send signal for others to output -- 
                cp      dir_delay	up_echo			        //dir_delay = up_echo (For master board output)
                call    output_echo	output_echo_radd		//output echo on master board (unique version for each board) 
                cp      0x80000002	const_num0			//set LED_GREEN on
		cp	s_data		const_num0			// send signal for other boards to know game has not ended
		call	send_driver	send_radd			
                be      end_turn	0			0

setw	        cp      0x80000002	const_num0			//set LED_GREEN off
		cp      s_data          const_num0                      //if no echo this turn, let boards know	
                call    send_driver     send_radd                       // call send driver to inform boards 
                sub	position	position	num_col		//w -> subtract number of columns from position

                be      grab_small1     maze_type       const_num0
grab_large1	cpfa	nTValueGrab	large_maze	position	//grab value of tile
                be      end_grab1       0               0
grab_small1     cpfa    nTValueGrab     small_maze      position
end_grab1	bne	end_game	nTValueGrab	const_num0	//If wall goto end
                cp      s_data		const_num0			//If not end, let board know game has not ended
                call    send_driver	send_radd			//call send driver
                cp      0x80000002	const_num0			//set LED_GREEN On
		be	end_turn	0		0

seta	        cp      0x80000002	const_num0			//set LED_GREEN off
		cp      s_data          const_num0                      //if no echo this turn, let boards know
                call    send_driver     send_radd                       // call send driver to inform boards
                sub	position	position	const_num1	//a -> subtract 1 from position
		be      grab_small2     maze_type       const_num0
grab_large2	cpfa	nTValueGrab	large_maze	position	//grab value of tile
                be      end_grab2       0               0
grab_small2     cpfa    nTValueGrab     small_maze      position
end_grab2       bne	end_game	nTValueGrab  	const_num0	//If wall goto end
                cp      s_data		const_num0			//If not end, let boards know game has not ended
                call    send_driver	send_radd			//call send driver
                cp      0x80000002	const_num0			//set LED_GREEN on
		be      end_turn	0		0

sets	        cp      0x80000002	const_num0			//set LED_GREEN off
		cp      s_data          const_num0                      //if no echo this turn, let boards know
                call    send_driver     send_radd                       // call send driver to inform boards
                add	position	position	num_col		//s -> add number of columns  to position
		be      grab_small3     maze_type       const_num0
grab_large3	cpfa	nTValueGrab	large_maze	position	//grab value of tile
                be      end_grab3       0               0
grab_small3     cpfa    nTValueGrab     small_maze      position
end_grab3	bne	end_game	nTValueGrab	const_num0	//If wall goto end
                cp      s_data		const_num0			//If not end, let boards know game has not ended 
                call    send_driver	send_radd
                cp      0x80000002	const_num0			//set LED_GREEN on
		be	end_turn	0		0

setd	        cp      0x80000002	const_num0			//set LED_GREEN off
		cp      s_data          const_num0                      //if no echo this turn, let boards know
                call    send_driver     send_radd                       // call send driver to inform boards
                add	position	position	const_num1	//d -> add 1 to position
		be      grab_small4     maze_type       const_num0
grab_large4	cpfa	nTValueGrab	large_maze	position	//grab value of tile
                be      end_grab4       0               0
grab_small4     cpfa    nTValueGrab     small_maze      position
end_grab4	bne	end_game	nTValueGrab	const_num0	//If wall goto end
                cp      s_data		const_num0			//If not end, let boards know game has not ended
                call    send_driver	send_radd				
                cp      0x80000002	const_num1                     	//set LED_GREEN on
		be	end_turn	0		0


end_game	cp	endgame		const_num1
                be      lose		nTValueGrab	const_num1
                be      finish		nTValueGrab	const_num2

lose            cp      ending_type	const_num1
                cp      s_data		const_num1
                call    send_driver	send_radd
                be      end_turn	0		0

finish          cp      ending_type	const_num2  
                cp      s_data		const_num2
                call    send_driver	send_radd
                be      end_turn	0		0

end_turn	ret	game_simulator_radd


nTValueGrab	        0
ending_type             0
endgame			0
game_simulator_radd     0

#include output_echo.e
#include keyboard_driver.e
#include distance_alg.e
#include send_driver.e
#include send_echoes.e
#include speaker_driver.e
#include echo_algorithm.e



